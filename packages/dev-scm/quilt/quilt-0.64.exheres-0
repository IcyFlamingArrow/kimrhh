# Copyright 2010 Wulf C. Krueger <philantrop@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require bash-completion

SUMMARY="Quilt allows you to easily manage large numbers of patches"
DESCRIPTION="
Quilt is a set of scripts to manage a series of patches by keeping track of the
changes each patch makes. Patches can be applied, un-applied, refreshed, etc.
The key philosophical concept is that your primary output is patches, not '.c'
files or '.h' files, so patches are the first-class object here. It was originally
based on Andrew Morton's patch scripts published on the Linux kernel mailing list
a while ago, but has been heavily modified since then.
"
HOMEPAGE="http://savannah.nongnu.org/projects/${PN}"
DOWNLOADS="mirror://savannah/${PN}/${PNV}.tar.gz"

REMOTE_IDS="freecode:${PN}"

UPSTREAM_CHANGELOG="http://git.savannah.gnu.org/cgit/${PN}.git/tree/${PN}.changes?id=v${PV} [[ lang = en ]]"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64 ~x86"

#RESTRICT="test"

DEPENDENCIES="
    build+run:
        dev-util/diffstat[>=1.47]
        sys-apps/ed
        virtual/mta
    run:
        mail-filter/procmail [[ note = [ mail.test fails without this ] ]]
    suggestion:
        media-gfx/graphviz[>=2.20.3] [[ description = [ required for dependencies graph visualization ] ]]
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/${PN}-0.60-Fix-prototype-mismatch-error.patch
    "${FILES}"/0001-Use-standard-sysconfdir-as-an-etcdir.patch
)
DEFAULT_SRC_INSTALL_PARAMS=( BUILD_ROOT="${IMAGE}" )
DEFAULT_SRC_INSTALL_EXTRA_DOCS=( quilt.changes )
DEFAULT_SRC_TEST_PARAMS=( -j1 )

src_install() {
    default

    rm -r "${IMAGE}"/etc/bash_completion.d || die "removing cruft failed"
    dobashcompletion bash_completion
}
