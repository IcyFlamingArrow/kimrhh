# Copyright 2008-2011 Kim Højgaard-Hansen <kimrhh@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.15 ] ] \
    freedesktop-desktop freedesktop-mime gtk-icon-cache

SUMMARY="Dia is a GTK+ based program for drawing structured diagrams"
DESCRIPTION="
Dia is inspired by the commercial Windows program 'Visio', though more geared towards informal
diagrams for casual use. It can be used to draw many different kinds of diagrams. It currently has
special objects to help draw entity relationship diagrams, UML diagrams, flowcharts, network
diagrams, and many other diagrams. It is also possible to add support for new shapes by writing
simple XML files, using a subset of SVG to draw the shape. It can load and save diagrams to a
custom XML format (gzipped by default, to save space), can export diagrams to a number of formats,
including EPS, SVG, XFIG, WMF and PNG, and can print diagrams (including ones that span multiple
pages).
"
HOMEPAGE="http://live.gnome.org/Dia"
DOWNLOADS="ftp://ftp.gnome.org/pub/gnome/sources/dia/$(ever range 1-2 ${PV})/${PNV}.tar.xz"

REMOTE_IDS="freecode:dia"

LICENCES="GPL-2"
SLOT="0"
MYOPTIONS="
    doc [[ description = [ Build Dia ps,pdf and html documentation ] ]]
    emf [[ description = [ Enable Enhanced Metafile support ] ]]
    gnome [[ description = [ Enable Gnome integration. Requires several Gnome deps ] ]]
    python-bindings [[ description = [ Build Dia Python bindings using Swig ] ]]
    python-plugin [[ description = [ Build Dia Python plugin ] ]]
"

#FIXME: test fails - assertion
RESTRICT="test"

#FIXME: maybe dia expects db2html not docbook2html from docbook-utils
DEPENDENCIES="
    build:
        dev-util/intltool[>=0.35.0]
        sys-devel/gettext
        virtual/pkg-config
    build+run:
        dev-libs/glib:2[>=2.6.0]
        dev-libs/libxml2:2.0[>=2.3.9]
        dev-libs/libxslt
        gnome-platform/libart_lgpl[>=2.0]
        media-libs/freetype:2[>=2.0.9]
        media-libs/libpng:=
        sys-libs/zlib
        x11-libs/cairo[>=1.0.0]
        x11-libs/gdk-pixbuf:2.0[>=2.0]
        x11-libs/gtk+:2[>=2.6.0]
        x11-libs/pango[>=1.8.0]
        doc? ( app-text/docbook-utils app-text/dblatex )
        emf? ( media-libs/libEMF )
        gnome? (
            gnome-platform/libgnomeui[>=2.0]
            gnome-platform/libgnome[>=2.0]
        )
        python-bindings? ( dev-lang/swig[>=1.3.31] )
        python-plugin? ( dev-lang/python:=[>=2.3.0] )
"

#cairo,freetype not optional since pango/gtk+ are needed
DEFAULT_SRC_CONFIGURE_PARAMS=(
    --with-cairo
    --with-freetype
)
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    'doc db2html'
    'emf libemf'
    'gnome'
)
DEFAULT_SRC_CONFIGURE_OPTION_WITHS=(
    'doc hardbooks'
    'python-bindings swig'
    'python-plugin python'
)

DEFAULT_SRC_INSTALL_EXCLUDE=( readme.win32 )

src_prepare() {
    # respect datarootdir
    edo sed \
        -e 's:itlocaledir = $(prefix)/$(DATADIRNAME)/locale:itlocaledir = $(datarootdir)/locale:' \
        -i po/Makefile.in.in

    autotools_src_prepare
}

src_install() {
    default

    edo find "${IMAGE}"/ -type d -empty -delete

    # install desktop file for integrated mode
    # https://bugzilla.gnome.org/show_bug.cgi?id=588208
    edo sed -e 's|^Exec=dia|Exec=dia --integrated|' \
            -e '/^Name/ s|$| (integrated mode)|' \
            "${IMAGE}"/usr/share/applications/dia.desktop \
            > "${IMAGE}"/usr/share/applications/dia-integrated.desktop
}

pkg_postinst() {
    freedesktop-desktop_pkg_postinst
    freedesktop-mime_pkg_postinst
    gtk-icon-cache_pkg_postinst
}

pkg_postrm() {
    freedesktop-desktop_pkg_postrm
    freedesktop-mime_pkg_postrm
    gtk-icon-cache_pkg_postrm
}

